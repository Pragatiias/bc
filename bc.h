#define PLUS 1
#define MINUS -1 
#define PI 3.1415926535
#include<math.h>
#define OPERAND 10
#define OPERATOR 20
#define VARIABLE 30
#define END     40
#define ERROR   50
#define PREINC  60
#define POSINC  70
#define PREDEC  80
#define POSDEC  90
#define MAX 1024
typedef struct node {
        int num;
        int flag;
        struct node *next, *prev;
}node;

typedef struct list {
        node *head, *tail;
        int len, dec, sign;
}list;

void copy(list *l1, list *l2);
int checkzero(list *l);
list *addnum(list *l1, list *l2);
list *subnum(list *l1, list *l2);
list *mulnum(list *l1, list *l2);
list *division(list *l1, list *l2);
list *modulus(list *l1, list *l2);
int compare(list *l1, list *l2);
list *power(list *l1, list *l2);
list *factorial(list *l1, list *l2);
int precedence(char op);
int readline(char *arr, int len);
list *infix(char *str);
static list *L[26][10];
void init(list *l);
void insert(list *l, int pos, int num);
int remov(list *l, int pos);
void traverse(list *l, int arg);
void append(list *l, int num);
int length(list *l);
int dec(list *l);
int show(list *l, int pos);
typedef struct stack{
	int i;
	list *l[MAX];

}stack;
void push(stack *s, list *l);
list *pop(stack *s);
int empty(stack *s);
int full(stack *s);
void sinit(stack *s);
typedef struct stack1{
	int i;
	list *l[MAX];

}stack1;
void push1(stack1 *s, list *l);
list *pop1(stack1 *s);
int empty1(stack1 *s);
int full1(stack1 *s);
void sinit1(stack1 *s);
typedef struct cstack{
	char a[MAX];
	int i;
}cstack;
void cpush(cstack *s, char num);
char cpop(cstack *s);
int cempty(cstack *s);
int cfull(cstack *s);
void cinit(cstack *s);
typedef struct token {
	char op;
	int dec;
	int type;
	list *l;
	int flag;
	char var;

}token;
token *getnext(char *, int *);
enum states { SPC, DIG, OPR, VAR, STOP, ERR };
