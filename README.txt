TITLE: Basic Calculator
NAME: Pragati Sanjay Zende
MIS:-111703064

Description:

	My bignum basic calculator does the basic operations such as multiplication, addition, subtraction, division, modulus, 
    on infinitely long real numbers.

    It also finds factorial of a long numbers.
    
    Power operation can also be done using this calculator.
    
    It does comparison operations also.
    
    Above operations can be done wirh infinitely long all real numbers.